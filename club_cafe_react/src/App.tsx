import './App.css'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Header from './components/Header'
import Home from './components/Home'
import Create from './components/Create'
import DetailEvent from './components/DetailEvent'

function App() {

  return (
    <div className="App">

      <BrowserRouter>
          <Header></Header>      
        <Routes>
          <Route path="/" element={<Home />}></Route>
          <Route path="/evenement/detail/:id" element={<DetailEvent />}></Route>
          <Route path="/create" element={<Create />}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App
