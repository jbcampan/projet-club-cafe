import { NavLink } from 'react-router-dom'


export default function Header() {

  return (
    <div className="navbar-custom w-100 text-white d-flex position-absolute">
        <div className="container-fluid d-flex justify-content-center align-items-center">
          <NavLink to="/" className="nav-link">
            Accueil
          </NavLink>
        </div>
    </div>
  )
}