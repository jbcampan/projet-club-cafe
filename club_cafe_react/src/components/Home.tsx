import { useState, useEffect } from 'react';
// import { NavLink } from 'react-router-dom'

import { FaArrowDown, FaPlus, FaArrowRight, FaArrowUp } from 'react-icons/fa';
import Evenement from './Evenement';
import Button from './Button';
import config from "../../config.json";


interface Evenement {
    nom: string;
    date: string;
    description: string;
}


export default function Home() {

    const a = [<FaArrowDown />, <FaPlus />]
    const [evenements, setEvenements] = useState<Evenement[]>([]);

    useEffect(() => {
        fetch(`${config.api}/api/evenements`)
            .then(response => response.json())
            .then(biens => setEvenements(biens['hydra:member']))
            .catch(error => console.error(error));
    }, []);

    console.log(evenements);



    return (
        <div>
            <div className="vh-100 bg-home-custom" style={{ backgroundImage: `url("/img/home2.jpg")`, backgroundSize: 'cover', backgroundPosition: "center" }}>
                <div className="bg-home-custom w-100 h-100">
                    <div className=" text-white h-100 d-flex justify-content-center align-items-center flex-column">
                        <h1 className="monoton-title display-1 text-capitalize mb-3">Roi des cafés</h1>
                        <h2 className="rubik h5 mb-5">Des cafés royaux, un service impérial.</h2>
                        <h3 className="fs-6 text-center my-3">Inscrivez-vous pour participer à nos différents évènements, <br></br> Organisez également vos propres évènements.</h3>
                        <div className='d-md-flex mt-4'>
                            <a href="#liste" className="nav-link button-rounded2 mx-5 my-4">
                                Participer <span className='ps-3 fw-bold'><FaArrowDown /></span>
                            </a>
                            <Button link="/create" classe="button-rounded2-green my-4" name="Proposer" icon={a[1]} />
                        </div>
                        <div className='position-absolute w-100 bottom-0 end-0 h-25 black-gradient-custom'></div>
                    </div>
                </div>
            </div>
            <section id="liste" className='bg-black vh-100 pt-5'>
                <h2 className='text-white text-center py-5 monoton-font display-2'>Événements à venir </h2>
                <div className='container text-center text-white'>
                    {evenements.map((evenement) =>
                        <Evenement evenement={evenement} />
                    )}
                </div>
            </section>
        </div>
    )
}