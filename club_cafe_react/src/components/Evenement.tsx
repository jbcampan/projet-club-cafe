import { FaArrowRight } from 'react-icons/fa';
import Button from './Button';

export default function Evenement({ evenement }: any) {

    const a = [<FaArrowRight />]

    function formatDate(dateString: string) {
        const months = [
            'janvier',
            'février',
            'mars',
            'avril',
            'mai',
            'juin',
            'juillet',
            'août',
            'septembre',
            'octobre',
            'novembre',
            'décembre',
        ];

        const date = new Date(dateString);
        const day = date.getDate();
        const monthIndex = date.getMonth();
        const year = date.getFullYear()
        const monthName = months[monthIndex];

        return `${day} ${monthName} ${year}`;
    }

    return (
        <div className='row border-bottom border-white py-5 d-flex align-items-center'>
            <div className='col-8 col-md-6 display-6 fw-medium py-3'>
                {evenement.nom}
            </div>
            <div className='col-4 col-md-3 fs-4'>
                {formatDate(evenement.date)}
            </div>
            <div className='col-12 col-md-3'>
                <Button link={`/evenement/detail/${evenement.id}`} classe="button-rounded3" name="Voir plus" icon={a[0]} />
            </div>
        </div>
    );
}

