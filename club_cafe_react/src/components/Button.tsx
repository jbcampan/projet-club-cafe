import { NavLink } from "react-router-dom";

interface ButtonProps {
    link?: string;
    classe?: string;
    name: string;
    icon: any;
}

const Button: React.FC<ButtonProps> = ({
    link,
    classe,
    icon,
    name
}) => {

    return (
        <NavLink to={`${link}`} className={`nav-link mx-5 ${classe}`}>
            {name} <span className='ps-3 fw-bold'>{icon}</span>
        </NavLink>
    );
}

export default Button;