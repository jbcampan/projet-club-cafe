import { FaArrowDown, FaPlus } from "react-icons/fa";
import { useState, useEffect } from 'react';
import config from "../../config.json";
import Button from "./Button";


// import Button from "./Button";

export default function Create() {

    const a = [<FaPlus />]
    

    const [nom, setNom] = useState('');
    const [date, setDate] = useState('');
    const [description, setDescription] = useState('');

    const handleSubmit = async (event: any) => {
        event.preventDefault();
        const data = { nom, date, description };
        const response = await fetch(`${config.api}/api/evenements`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        console.log(await response.json());

        window.location.href = '/';

    }


    return (

        <div className="vh-100" style={{ backgroundImage: `url("/img/home2.jpg")`, backgroundSize: 'cover', backgroundPosition: "center" }}>
            <div className="bg-home-custom w-100 h-100">
                <div className='bg-detail-custom w-100 h-100'>
                    <div className=" text-white h-100 d-flex justify-content-center align-items-center flex-column">


                        <section id="contact" className="position-absolute top-50 start-50 translate-middle my-4 px-4 w-100 z-3">
                            <h1 className="text-center py-4 text-white fw-light monoton" >Créez un nouvel évènement</h1>

                            <div className="container py-4">
                                <div className="row">
                                    <div className="col-12 col-md-6 offset-md-3 col-lg-4 offset-lg-4">
                                        <form onSubmit={handleSubmit}>

                                            <div className="pb-1 text-dark">
                                                <div className="mb-4 form-floating">
                                                    <input type="text" name="nom" id="nom" className="form-control" placeholder="Nom" value={nom} onChange={(e) => setNom(e.target.value)} />
                                                    <label htmlFor="nom">Nom</label>
                                                </div>
                                                <div className="mb-4 form-floating">
                                                    <input type="date" name="date" id="date" className="form-control" placeholder="Date" value={date} onChange={(e) => setDate(e.target.value)} />
                                                    <label htmlFor="date">Date</label>
                                                </div>
                                                <div className="mb-4 form-floating">
                                                    <input type="text" name="description" id="description" className="form-control" placeholder="Description" value={description} onChange={(e) => setDescription(e.target.value)} />
                                                    <label htmlFor="description">Description</label>
                                                </div>
                                            </div>


                                            {/* <Button link="/" classe="button-rounded2-green text-center mt-5" name="Proposer" icon={a[0]} /> */}
                                            <div className="d-flex justify-content-center">
                                                <button type="submit" className="button-rounded2-green nav-link mx-5 text-center mt-5">proposer<span className='ps-3 fw-bold'><FaPlus/></span></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <div className='position-absolute w-100 bottom-0 end-0 h-25 black-gradient-custom'></div>
                    </div>
                </div>
            </div>
        </div>

    )
}