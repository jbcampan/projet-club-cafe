import { useParams } from 'react-router-dom';
import { useState, useEffect } from 'react';

import config from "../../config.json";
import Button from './Button';
import { FaArrowDown } from 'react-icons/fa';

interface Evenement {
    nom: string;
    date: string;
    description: string;
}


export default function DetailEvent() {

    const { id } = useParams();

    function formatDate(dateString: string) {
        const months = [
            'janvier',
            'février',
            'mars',
            'avril',
            'mai',
            'juin',
            'juillet',
            'août',
            'septembre',
            'octobre',
            'novembre',
            'décembre',
        ];

        const date = new Date(dateString);
        const day = date.getDate();
        const monthIndex = date.getMonth();
        const year = date.getFullYear()
        const monthName = months[monthIndex];

        return `${day} ${monthName} ${year}`;
    }

    const a = [<FaArrowDown />]
    const [evenement, setEvenement] = useState<any>({});

    useEffect(() => {
        fetch(`${config.api}/api/evenements/${id}`)
            .then(response => response.json())
            .then(evenement => setEvenement(evenement))
            .catch(error => console.error(error));
    }, []);

    const [prenom, setPrenom] = useState('');
    const [nom, setNom] = useState('');
    const [numero, setNumero] = useState('');

    const handleSubmit = async (event: any) => {
        event.preventDefault();
        const data = { prenom, nom, numero };
        const response = await fetch(`${config.api}/api/participants`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });
        console.log(await response.json());

        window.location.href = '/';

    }

    console.log(evenement);

    return (
        <div className="vh-100" style={{ backgroundImage: `url("/img/home2.jpg")`, backgroundSize: 'cover', backgroundPosition: "center" }}>
            <div className="bg-home-custom w-100 h-100">
                <div className='bg-detail-custom w-100 h-100'>
                    <div className=" text-white h-100 d-flex justify-content-center align-items-center flex-column">
                        <div className='container'>
                            <div className='row'>
                                <div className="col-12 col-lg-6">
                                    <h1 className="text-center py-2 text-white fw-light monoton" >{evenement.nom}</h1>
                                    <div className="container py-4">
                                        <div className="row text-center">
                                            <div className="col-12 col-md-6 offset-md-3 col-lg-10 offset-lg-1">
                                                <p className='fs-3'>{formatDate(evenement.date)}</p>
                                                <p>{evenement.description}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 col-lg-6 z-3">
                                    <div className="text-center pb-4 text-white fw-light monoton" ></div>
                                    <div className="container py-4">
                                        <div className="row">
                                            <div className="col-12 col-md-6 offset-md-3 col-lg-6 offset-lg-3">
                                                <form onSubmit={handleSubmit}>
                                                    <div className="pb-1 text-dark">
                                                        <div className="mb-4 form-floating">
                                                            <input type="text" name="prenom" id="prenom" className="form-control" placeholder="Prénom" value={prenom} onChange={(e) => setPrenom(e.target.value)} />
                                                            <label htmlFor="prenom">Prenom</label>
                                                        </div>
                                                        <div className="mb-4 form-floating">
                                                            <input type="text" name="nom" id="nom" className="form-control" placeholder="Nom" value={nom} onChange={(e) => setNom(e.target.value)} />
                                                            <label htmlFor="nom">Nom</label>
                                                        </div>
                                                        <div className="mb-4 form-floating">
                                                            <input type="text" name="numero" id="numero" className="form-control" placeholder="Numéro" value={numero} onChange={(e) => setNumero(e.target.value)} />
                                                            <label htmlFor="numero">Numéro</label>
                                                        </div>
                                                    </div>
                                                    {/* <Button link="/" classe="button-rounded2 text-center mt-5" name="Participer" icon="" /> */}
                                                    <div className="d-flex justify-content-center">
                                                        <button type="submit" className="button-rounded2 nav-link mx-5 text-center mt-5">Participer<span className='ps-3 fw-bold'></span></button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='position-absolute w-100 bottom-0 end-0 h-25 black-gradient-custom'></div>
                    </div>
                </div>
            </div>
        </div>
    )
}