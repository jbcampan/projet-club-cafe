# Readme du projet Club Café

ROI DES CAFES

Ce projet est une application web développée avec React et Symfony pour une application qui permet de créer des évènements dans un café, et de signaler sa participation à des évènements.


INSTALLATION

Clonez ce dépôt sur votre machine
Installez les dépendances PHP en exécutant composer install dans le répertoire racine de l'application
Configurez la base de données dans le fichier .env en renseignant les informations de connexion à votre base de données
Créez la base de données en exécutant php bin/console doctrine:database:create
Mettez à jour la base de données en exécutant php bin/console doctrine:schema:update --force
Installez les dépendances JavaScript en exécutant npm install dans le répertoire client
Lancez l'application en exécutant npm run dev dans le répertoire client et symfony server:start dans le répertoire racine de l'application
Accédez à l'application dans votre navigateur à l'adresse http://localhost:3000


UTILISATION

La page d'accueil permet de consulter la liste des évènements à venir.
Elle donne accès à des pages de détail de chaque évènements, avec un formulaire d'inscription. 
Depuis la page d'accueil il est également possible d'avoir accès à la page de création d'évènements via un formulaire.


